export interface produtosLista {
    id: number;
    dataImportacao: string;
    quantidade: number;
    dataEntrega: string;
    total: number;
}


import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: LoginService,
    private toastrLogin: ToastrService
  ) { }



  form: FormGroup;
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  async onSubmit() {
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const username = this.form.get('username').value;
        const password = this.form.get('password').value;
        const result = this.accountService
          .login({
            login: username,
            senha: password,
          })
          .subscribe(
            (result) => {
              window.sessionStorage.setItem('tokenAuth', result.jwt);
              this.router.navigate(['']);
              this.toastrLogin.success('Você está logado', `Olá ${result.usuario.login}`);

            },
            (error) => {
              this.toastrLogin.error('Desculpe não foi possivel realizar', 'Login');
            }
          );
      } catch (err) {
        this.loginInvalid = true;
        console.error(err);
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }


}

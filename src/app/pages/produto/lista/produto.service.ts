import { produtosLista } from './../../../models/produtos/produtosLista.model';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  constructor(private http: HttpClient) { }

  listaprodutos(): Observable<produtosLista[]> {
    return this.http.get<produtosLista[]>(`${environment.api}produtos/v1/get`);
  }


  detalheprodutos(id:number): Observable<produtosLista[]> {
    return this.http.get<produtosLista[]>(`${environment.api}produtos/v1/get/${id}`);
  }

}

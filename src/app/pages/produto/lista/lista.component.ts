import { ProdutoService } from './produto.service';
import { Component, OnInit } from '@angular/core';
import { produtosLista } from 'src/app/models/produtos/produtosLista.model';
import { DetalhesCadastroComponent } from 'src/app/components/detalhes-cadastro/detalhes-cadastro.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  constructor(private produtoService: ProdutoService, public modalDetalhe: MatDialog, private toastr: ToastrService) { }

  listaProdutos: produtosLista[]
  load = false

  ngOnInit(): void {
    this.carregarProdutos()
  }





  detalherProdutos(id: number) {
    this.load = true
    this.produtoService.detalheprodutos(id).subscribe(
      (sucesso) => {
        const dialogRefdados = this.modalDetalhe.open(DetalhesCadastroComponent, {
          minWidth: '750px',
          data: sucesso
        });

        dialogRefdados.afterClosed().subscribe(result => {
          console.log('The dialog was closed');

        });
    this.load = false

      },
      (error) => {
        console.log(error)
        this.load = false
        this.toastr.error('Desculpe, ocorreu um erro interno', 'Detalhes de produto');

      }
    )
  }


  carregarProdutos() {
    this.load = true
    this.produtoService.listaprodutos().subscribe(
      (sucesso) => {
        this.listaProdutos = sucesso
        this.load = false
      },
      (error) => {
        console.log(error)
        this.load = false

      }
    )
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  constructor(private http: HttpClient) { }


  registro(user: any): Observable<any> {
    return this.http.post<any>(`${environment.api}usuarios/v1/post`, user);
  }


}

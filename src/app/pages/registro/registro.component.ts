import { RegistroService } from './registro.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private registroService: RegistroService,
    private toastrRegistro: ToastrService
  ) { }



  form: FormGroup;
  public registroInvalid: boolean;
  private formSubmitAttempt: boolean;

  ngOnInit(): void {

    this.form = this.fb.group({
      login: ['', Validators.required],
      senha: ['', Validators.required],
    });



  }



  async onSubmit() {
    this.registroInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const username = this.form.get('login').value;
        const password = this.form.get('senha').value;
        const result = this.registroService
          .registro({
            login: username,
            senha: password,
          })
          .subscribe(
            (result) => {
              this.router.navigate(['login']);
            },
            (error) => {
              this.toastrRegistro.error('Desculpe, ocorreu um erro interno', 'Cadastro de usuario');

            }
          );
      } catch (err) {
        this.registroInvalid = true;
        console.error(err);
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }







}

import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { produtosLista } from 'src/app/models/produtos/produtosLista.model';

@Component({
  selector: 'app-detalhes-cadastro',
  templateUrl: './detalhes-cadastro.component.html',
  styleUrls: ['./detalhes-cadastro.component.css']
})
export class DetalhesCadastroComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: produtosLista[], public dialogRefDetalhes: MatDialogRef<DetalhesCadastroComponent>,) { }

  ngOnInit(): void {
 
  }
  
  cancel() {
    this.dialogRefDetalhes.close(true);

  }

}

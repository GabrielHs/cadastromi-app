/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ImportacaoService } from './importacao.service';

describe('Service: Importacao', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImportacaoService]
    });
  });

  it('should ...', inject([ImportacaoService], (service: ImportacaoService) => {
    expect(service).toBeTruthy();
  }));
});

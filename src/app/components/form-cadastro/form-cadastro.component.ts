import { ImportacaoService } from './importacao.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-form-cadastro',
  templateUrl: './form-cadastro.component.html',
  styleUrls: ['./form-cadastro.component.css']
})
export class FormCadastroComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<FormCadastroComponent>,
    private fb: FormBuilder,
    private importacaoService: ImportacaoService
  ) { }

  arquivo: File = null;


  ngOnInit(): void {
  }

  onSelectFile(fileInput: any) {
    this.arquivo = <File>fileInput.target.files[0];
  }


  cancel() {
    this.dialogRef.close(true);

  }

  importar() {
    let formData = new FormData();
    formData.append('Arquivo', this.arquivo);
    this.importacaoService.importarExel(formData).subscribe(
      (sucesso) => { },
      (erro) => { }
    );

    this.cancel();
    this.arquivo = null
    window.location.reload();
  }



}

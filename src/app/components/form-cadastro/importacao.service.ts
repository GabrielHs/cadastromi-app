import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImportacaoService {

  constructor(private http: HttpClient) { }

  importarExel(arquivo:FormData) {
     return this.http.post(`${environment.api}produtos/v1/post`, arquivo);
  }

}

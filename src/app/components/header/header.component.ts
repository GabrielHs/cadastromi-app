import { FormCadastroComponent } from './../form-cadastro/form-cadastro.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginService } from 'src/app/pages/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private loginService: LoginService, private router: Router
  ) { }

  ngOnInit(): void {
  }

  addProdutos(): void {
    const dialogRef = this.dialog.open(FormCadastroComponent, {
      minWidth: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }




  Logout(){
    this.loginService.logoutJwt()
    this.router.navigate(['/login'])
  }


}

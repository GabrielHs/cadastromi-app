import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './login/shared/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { ListaComponent } from './pages/produto/lista/lista.component';
import { RegistroComponent } from './pages/registro/registro.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ListaComponent,
    canActivate: [AuthGuard]

  },
  {
    path: 'registro',
    component: RegistroComponent,

  },
  {
    path: 'login',
    component: LoginComponent,
  },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
